FROM nginx:stable
COPY site.conf /etc/nginx/conf.d/default.conf
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"] 
EXPOSE 80
